Integracao NodeJS SOAP
======================


Exemplo de integracao a api SOAP de Pre Postagem do Portal Postal.



Instruções
----------


Alterar a variavel host para:
  - Ambiente de Teste: 
        var host = http://sandbox.portalpostal.com.br/
  - Ambiente de Produção: 
        var host = https://portalpostal.com.br/

Documentação da API:

* Com sequenquencia lógica: o sistema ERP JÀ POSSUI o SRO antes de gerar a PreVenda
    https://scc4.atlassian.net/wiki/spaces/APP/pages/5439489
* Sem sequenquencia lógica: o sistema ERP NÃO POSSUI o SRO antes de gerar a PreVenda
    https://scc4.atlassian.net/wiki/spaces/APP/pages/5242898


Execução

    npm instal
    node index.js


