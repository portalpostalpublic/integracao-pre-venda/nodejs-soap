var soap = require('soap');
var js2xmlparser = require("js2xmlparser");
  
  // Host de producao
  // var host= 'https://portalpostal.com.br/';

  var host= 'http://sandbox.portalpostal.com.br/';


  var url =  host + 'axis2/services/PrePostagemWS?wsdl';

  var xmlObj = {
   pre_postagem: {
      nome: 'ELAINE SANTOS',
      endereco: 'AVENIDA.DOUTOR DEMOSTENES RIO BRANCO',
      bairro: 'VILA NOVA',
      numero: '790',
      complemento: 'CASA',
      cidade: 'SANTA RITA DE CALDAS',
      estado: 'MG',
      cep: '37775000',
      chave: '37158205',
      nota_fiscal: '329327',
      cpf_cnpj: '06036672608',
      celular: '(035) 998982945',
      email: 'elainemaria.em209@gmail.com',
      conteudo: [
         {
            descricao: 'XXXXXXXXXXXXXXXXX',
            quantidade: 1,
            valor: '75,96'
         },
         {
            descricao: 'XXXXXXXXXXXXXXXXX',
            quantidade: '1',
            valor: '37,98'
         },
         {
            descricao: 'XXXXXXXXXXXXXXXXX',
            quantidade: '1',
            valor: '37,98'
         },
         {
            descricao: 'XXXXXXXXXXXXXXXXX',
            quantidade: '1',
            valor: '37,98'
         }
      ],
      servico: 'SEDEX',
      servico_adicional: 'ARVD',
      valor_declarado: '202,90'
   }
} 


  var args = {
	  	xml: '<![CDATA['.concat(js2xmlparser.parse("portalpostal", xmlObj)).concat(']]>'),
		codAgencia: 1,
		login: 'sandbox',
		senha: '123456' 
};

  

  var soapEndPoint = host +'axis2/services/PrePostagemWS.PrePostagemWSSOAP11port_http/';

    console.log('Enviando requisição');
    soap.createClient(url,function(err,client) {
    	client.setEndpoint(soapEndPoint);
  		client.PrePostagemXml(args, function(err,result){
	       if (err) {
	        	console.log(err);
	   	   } else if (result) {	   	   		
	       		console.log(result);
 		   } else {
 		   		console.log('Erro na requisicao.')
 		   }	       

  		})

  	})



